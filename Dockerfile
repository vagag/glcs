FROM golang:1.17

WORKDIR /workspace/glcs
RUN git clone https://gitlab.com/vagag/glcs .

RUN go mod tidy
RUN go build ./cmd/glcs

ENV UPCXX_INSTALL="/shared-workspace/libs/upcxx"
ENV PGASGRAPH_INSTALL="/shared-workspace/pgasgraph"
ENV PATH="${UPCXX_INSTALL}/bin:${PGASGRAPH_INSTALL}/build/src/PGASGraphCLI:$PATH"

ARG HOST=
ENV HOST $HOST

ARG PORT=30001
ENV PORT $PORT

EXPOSE $PORT

CMD ./glcs --http=$HOST:$PORT
